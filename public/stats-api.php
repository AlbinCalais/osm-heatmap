<?php

/*

Dev notes:

This api will return the data for a chart like https://resultmaps.neis-one.org/osm-notes
but that can be generated for a specific area (a bounding box).

This api *does not* take in account reopened notes.

*/

include 'params.php';

/* Returns a list of opened or closed note counts for dates since some date */
function FetchNoteChangeCounts($dateRow, $excludeOpened, $queryArgs, $querySettings) {
  global $connection;

  $precisionLevel = $querySettings['highDatePrecision'] ? "week" : "month";
  $query = $connection->prepare("
    select 
      count(*) as count,
      year($dateRow) as year,
      $precisionLevel($dateRow) as $precisionLevel
    from notes
    where $dateRow > :since "
    .( $excludeOpened ? "and not opened " : "" )
    .( $querySettings['hasbbox'] ? 
    "and latitude >= 1*:bboxY1
     and latitude <= 1*:bboxY2
     and ((
       (1*:bboxX1 < 1*:bboxX2) and longitude>=1*:bboxX1 and longitude<=1*:bboxX2 
     ) or (
       (1*:bboxX1 >= 1*:bboxX2) and (longitude>=1*:bboxX1 or longitude<=1*:bboxX2)
     )) " : "" )
    ."group by year($dateRow), $precisionLevel($dateRow);"
  );
  $query->execute($queryArgs);
  return $query->fetchAll(PDO::FETCH_ASSOC);
}

/* Returns the number of opened & closed notes at a given date */
function FetchCountAtDate($queryArgs, $querySettings) {
  global $connection;

  $query = $connection->prepare("
    select 
      sum(case when created_at < :since then 1 else 0 end) as openedCount,
      sum(case when closed_at  < :since then 1 else 0 end) as closedCount
    from notes
    where 1 "
    .( $querySettings['hasbbox'] ? 
    "and latitude >= 1*:bboxY1
     and latitude <= 1*:bboxY2
     and ((
       (1*:bboxX1 < 1*:bboxX2) and longitude>=1*:bboxX1 and longitude<=1*:bboxX2 
     ) or (
       (1*:bboxX1 >= 1*:bboxX2) and (longitude>=1*:bboxX1 or longitude<=1*:bboxX2)
     )) " : "" )
  );
  $query->execute($queryArgs);
  $results = $query->fetch(PDO::FETCH_ASSOC);
  return $results;
}

function GetStats() {

  $queryArgs = [];
  $querySettings = [
    'highDatePrecision' => false, // dictates which of "month" or "week" will be used
    'hasbbox' => false,
  ];

  $dateSince = '2013-04-24'; // the first osm note, hard coded to avoid a sql query
  $queryArgs['since'] = $dateSince;

  if(isset($_GET['since'])) {
    $dateSince = ValidateFormat($_GET, 'since' , DATE_REGEX);
    $queryArgs['since'] = $dateSince;
    // use high precision if there is less than one year between now and dateSince
    if($dateSince > date("Y-m-d", time() - 60*60*24*365)) {
      $querySettings['highDatePrecision'] = true;
    }
  }

  if(isset($_GET['bbox'])) {
    // dirty regex
    $bbox = ValidateFormat($_GET, 'bbox', "/^(-?\d{1,20}(?:.\d{1,20})?),(-?\d{1,20}(?:.\d{1,20})?),(-?\d{1,20}(?:.\d{1,20})?),(-?\d{1,20}(?:.\d{1,20})?)$/");
    array_shift($bbox);
    $bbox = array_map('floatval', $bbox);
    $queryArgs['bboxX1'] = $bbox[0];
    $queryArgs['bboxY1'] = $bbox[1];
    $queryArgs['bboxX2'] = $bbox[2];
    $queryArgs['bboxY2'] = $bbox[3];
    $querySettings['hasbbox'] = true;
  }

  $stats = [];
  $stats['date'] = $dateSince;
  $stats['highDatePrecision'] = $querySettings['highDatePrecision'];
  $stats['opened'] = FetchNoteChangeCounts('created_at', false, $queryArgs, $querySettings);
  $stats['closed'] = FetchNoteChangeCounts('closed_at', true, $queryArgs, $querySettings);
  $countsAtDate = FetchCountAtDate($queryArgs, $querySettings);
  $stats['openedAtDate'] = $countsAtDate['openedCount'];
  $stats['closedAtDate'] = $countsAtDate['closedCount'];

  return $stats;
}

echo json_encode(GetStats());

?>