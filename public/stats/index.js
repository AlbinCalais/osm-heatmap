
const API_URL = '../stats-api.php';

const loadingPage = document.getElementById('loading-section');
const contentPage = document.getElementById('content');
const errorPage = document.getElementById('error-section');
const currentURLLabel = document.getElementById('current-url-label');
const sinceDateInput = document.getElementById('since-date-input');
const sinceDateReload = document.getElementById('since-date-reload');
const mapCanvas = document.getElementById('map-canvas');

// set the dimensions and margins of the graph
const margin = {top: 10, right: 30, bottom: 30, left: 60},
    width = 860 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;
const colors = [ "#66c2a5", "#fc8d62", "#ddd6" ];

function createGraph(elem) {
  let svg = d3.select(elem)
    .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .attr("overflow", "visible")
      .append("g")
        .attr("transform", `translate(${margin.left},${margin.top})`);
  return svg;
}

function setGraphData(svg, labels, curves) {
  let yAxisMax = Math.max(...curves.flat().map(x => x.value));
  const x = d3.scaleTime()
    .domain(d3.extent(curves[0], c => c.key))
    .range([ 0, width ]);
  const y = d3.scaleLinear()
    .domain([ 0, yAxisMax * 110/100 ])
    .range([ height, 0 ]);

  // axes
  svg.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x));
  svg.append("g")
    .call(d3.axisLeft(y))
    .call(g => g.selectAll(".tick line").clone()
      .attr("x2", width)
      .attr("stroke-opacity", 0.1));

  let tooltip; // the tooltip is created after the curves to appear above them

  // actual curves
  for(let i = 0; i < curves.length; i++) {
    svg.append("path")
      .datum(curves[i])
      .attr("fill", "none")
      .attr("stroke", colors[i])
      .attr("stroke-width", 1.5)
      .attr("d", d3.line()
        .x(d => x(d.key))
        .y(d => y(d.value))
      );
    
    svg.append("g")
      .selectAll("dot")
      .data(curves[i])
      .enter()
      .append("circle")
      .attr("cx", d => x(d.key))
      .attr("cy", d => y(d.value))
      .attr("r", 4)
      .attr("fill", colors[i])
      .on('mouseleave', () => tooltip.style("display", "none"))
      .on('mouseover', function(ev, d) {
        let date = d.key;
        let count = d.value;
        let dateText = ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + date.getFullYear();
        tooltip
          .html(`${dateText}: ${count}`)
          .style("display", "initial")
          .attr("x", x(d.key) + 10)
          .attr("y", y(d.value) + 10);
      });
  }

  // legend
  const legend = svg.append("g")
    .attr("font-family", "sans-serif")
    .attr("font-size", 10)
    .selectAll("g")
    .data(labels)
    .enter().append("g")
      .attr("transform", (d, i) => `translate(${width*4/100},${i * 20})`);
  legend.append("rect")
    .attr("x", 0)
    .attr("width", 19)
    .attr("height", 19)
    .attr("fill", (d, i) => colors[i]);
  legend.append("text")
    .attr("x", 24)
    .attr("y", 9.5)
    .attr("dy", "0.32em")
    .text(d => d);

  tooltip = svg.append("text")
    .style("display", "none")
    .style("pointer-events", "none")
    .style("font-family", "monospace");

  svg.append("g")
    .selectAll("rect")
    .data([ 0, 97/100*width ])
    .enter().append("rect")
    .attr("x", d => d)
    .attr("y", 0)
    .attr("width", width*3/100)
    .attr("height", height)
    .attr("fill", colors[2]);
}

function adaptApiData(rawData) {
  let  { opened, closed, highDatePrecision, openedAtDate, closedAtDate } = rawData;
  let openCloseChartData = [];
  let totalChartData = [];
  let dateDataMap = {};

  function getEntry(item) {
    let dataValue;
    let itemDate = rawData.highDatePrecision ?
      new Date(item.year, 0, item.week*7) :
      new Date(item.year, item.month, 0);
    let dateKey = rawData.highDatePrecision ?
      `${item.year}-${item.week}` :
      `${item.year}-${item.month}`;
    if(!(dateKey in dateDataMap)) {
      dataValue = {
        date: itemDate,
        openedCount: 0,
        closedCount: 0,
      };
      dateDataMap[dateKey] = dataValue;
      openCloseChartData.push(dataValue);
    } else {
      dataValue = dateDataMap[dateKey];
    }
    return dataValue;
  }

  for(let o of opened)
    getEntry(o).openedCount = parseInt(o.count);
  for(let c of closed)
    getEntry(c).closedCount = parseInt(c.count);

  openCloseChartData.sort((d1, d2) => d1.date > d2.date);

  let prevOpenedCount = parseInt(openedAtDate);
  let prevClosedCount = parseInt(closedAtDate);
  for(let d of openCloseChartData) {
    prevOpenedCount += d.openedCount;
    prevClosedCount += d.closedCount;
    totalChartData.push({
      date: d.date,
      openedCount: prevOpenedCount,
      closedCount: prevClosedCount,
    });
  }

  return {
    highDatePrecision,
    openCloseChartData,
    totalChartData,
  };
}

async function fetchApiData(requestParams) {
  let url = API_URL;
  let params = "";
  for(let key in requestParams) {
    let value = requestParams[key];
    if(value !== undefined)
      params += `&${key}=${encodeURIComponent(value)}`;
  }
  if(params.length > 0)
    url += "?" + params.substring(1);
  let xhr = new XMLHttpRequest();
  xhr.open('GET', url);
  xhr.send();
  return new Promise((resolve, reject) => {
    xhr.onload = () => {
      if(xhr.status != 200) {
        reject(xhr.responseText);
      } else {
        try {
          resolve(JSON.parse(xhr.responseText));
        } catch (e) {
          reject(e instanceof SyntaxError ? "Invalid server response" : e.message);
        }
      }
    }
  });
}

function getApiParamsFromText(queryString) {
  let params = new URLSearchParams(queryString);
  let apiParams = {};
  if(params.has('bbox'))
    apiParams['bbox'] = params.get('bbox');
  if(params.has('since'))
    apiParams['since'] = params.get('since');
  return apiParams;
}

// setup inputs
window.addEventListener('load', () => {
  let urlApiParams = getApiParamsFromText(window.location.search);
  let currentUrl = window.location.toString();

  sinceDateInput.value = urlApiParams.since;
  sinceDateReload.onclick = () => {
    // reload with a difference date
    if(sinceDateInput.text != '')
      document.location = `?bbox=${urlApiParams.bbox}&since=${sinceDateInput.value}`;
  };
  
  currentURLLabel.innerText = currentUrl;
  currentURLLabel.onclick = () => {
    navigator.clipboard.writeText(currentUrl);
    currentURLLabel.innerText = Translation.getTranslation('copied_to_clipboard');
    setTimeout(() => currentURLLabel.innerText = currentUrl, 1500);
  }
});

// add a map view of the covered area
window.addEventListener('load', () => {
  let urlApiParams = getApiParamsFromText(window.location.search);
  let bbox = urlApiParams.bbox.split(',');

  let bboxw = bbox[2]-bbox[0];
  let bboxh = bbox[3]-bbox[1];
  if(bboxw < 0) bboxw += 360;
  let bboxBounds = [
    [ bbox[1], bbox[0] ],
    [ bbox[3], bbox[2] ],
  ];

  let baseLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, Tiles © <a href="https://operations.osmfoundation.org/policies/tiles/">OSMF</a>',
  });

  mapCanvas.style.aspectRatio = bboxw/bboxh; // not exact because of the planar projection

  let map = new L.Map(mapCanvas, {
    layers: [ baseLayer ],
    attributionControl: false,
    zoomControl: false,
    dragging: false,
    scrollWheelZoom: false,
  });

  map.fitBounds(bboxBounds);
  L.rectangle(bboxBounds, {color: "#ff7800", weight: 1}).addTo(map);
});

// fetch data and create graphs
window.addEventListener('load', async () => {
  try {
    let totalCountsChart = createGraph(document.getElementById("total-counts-chart"));
    let evolutionCountsChart = createGraph(document.getElementById("evolution-counts-char"));

    // fetch and adapt the api data into a useful one (filling gaps, scanning forward in time...)
    let urlApiParams = getApiParamsFromText(window.location.search);
    let rawData = await fetchApiData(urlApiParams);
    let adaptedData = adaptApiData(rawData);
    let { highDatePrecision, openCloseChartData, totalChartData } = adaptedData;
    
    let totalCountLabels = [
      Translation.getTranslation("notes_count").replace('{}', Translation.getTranslation("opened")),
      Translation.getTranslation("notes_count").replace('{}', Translation.getTranslation("closed")),
      Translation.getTranslation("low_confidance"),
    ];
    let evolutionCountLabels = [
      Translation.getTranslation("opened_notes_per") + Translation.getTranslation(highDatePrecision ? "week" : "month"),
      Translation.getTranslation("closed_notes_per") + Translation.getTranslation(highDatePrecision ? "week" : "month"),
      Translation.getTranslation("low_confidance"),
    ];

    // actually display the graphs
    setGraphData(totalCountsChart, totalCountLabels, [
      totalChartData.map(({ date, openedCount, closedCount }) => ({ key: date, value: openedCount-closedCount })), // number of still opened notes
      totalChartData.map(({ date, openedCount, closedCount }) => ({ key: date, value: closedCount })),             // number of closed notes
    ]);
    setGraphData(evolutionCountsChart, evolutionCountLabels, [
      openCloseChartData.map(({ date, openedCount, closedCount }) => ({ key: date, value: openedCount })), // per week/month evolution (growth)
      openCloseChartData.map(({ date, openedCount, closedCount }) => ({ key: date, value: closedCount })), // per week/month evolution (descrease)
    ]);

    contentPage.classList.remove('hidden');
  } catch (e) {
    errorPage.innerText = Translation.getTranslation("cannot_generate_stats") + ":\n" + e.toString();
    errorPage.classList.remove('hidden');
  } finally {
    loadingPage.classList.add('hidden');
  }
});

