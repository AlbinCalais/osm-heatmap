const PANNING_AUTO_UPDATE_DELAY = .5; // time (in seconds) to wait after the lastest movement before updating the heatmap
const API_URL = 'api.php';  // the heatmap api endpoint url
const STATS_PAGE_URL = 'stats/';
const HEATMAP_LAYER_PERSISTANCE = 3;  // when auto-reload is disabled, the heatmap layer is hidden after a zoom greater than the persistance

const DEBUG_MODE = new URLSearchParams(window.location.search).has('debug');

const ONLY = "only";
const EXCLUDED = "excluded";

var map = null;
var heatmapLayer = null;
var heatmapActiveLayerIndex = undefined;
var searchConfig = {
  precision: 5,              // 1..10, larger values take more samples
  openedNotes: ONLY,         // '', ONLY or EXCLUDED
  anonymNotes: '',    // '', ONLY or EXCLUDED
  commentedNotes: '', // '', ONLY or EXCLUDED
  createdBefore: undefined,  // date
  createdAfter: undefined,   // date
  automaticReload: true,     // bool
};

function clampPositionIn180Range({ lat, lng }) {
  lng %= 360;
  if(lng <= -180) lng += 360;
  if(lng >  +180) lng -= 360;
  return { lat, lng };
}

function getHeatmap(requestParams) {
  let xhr = new XMLHttpRequest();
  let url = API_URL;
  let params = "";
  for(let key in requestParams) {
    let value = requestParams[key];
    if(value !== undefined)
      params += `&${key}=${encodeURIComponent(value)}`;
  }
  if(params.length > 0) url += "?" + params.substring(1);
  xhr.open("GET", url);
  xhr.send();
  return new Promise((resolve, reject) => {
    xhr.onload = () => {
      if(xhr.status != 200) {
        reject(xhr.responseText);
      } else {
        try {
          resolve(JSON.parse(xhr.responseText));
        } catch (e) {
          reject(e instanceof SyntaxError ? "Invalid server response" : e.message);
        }
      }
    }
  });
}

async function reloadHeatmap(cancelable) {
  let bounds = map.getBounds();
  // let [ west, south, east, north ] = [ bounds.getWest(), bounds.getSouth(), bounds.getEast(), bounds.getNorth() ];
  // [ west, south, east, north ] = clampBBoxIn180Range([ west, south, east, north ]);
  // let bbox = L.latLngBounds(L.latLng(south, west), L.latLng(north, east)).toBBoxString();
  let dlon = (bounds.getEast() -bounds.getWest() ) / 20 / searchConfig.precision;
  let dlat = (bounds.getNorth()-bounds.getSouth()) / 10 / searchConfig.precision;
  let SW = clampPositionIn180Range(bounds.getSouthWest());
  let NE = clampPositionIn180Range(bounds.getNorthEast());
  let bbox = `${SW.lng},${SW.lat},${NE.lng},${NE.lat}`;
  let zoom = map.getZoom();
  console.log("loading heatmap","zoom:",zoom,"p:",searchConfig.precision,"dlon:",dlon,"dlat:",dlat);
  let response;
  try {
    response = await getHeatmap({
      zoom,
      dlon, dlat, bbox, 
      openednotes: searchConfig.openedNotes,
      anonymnotes: searchConfig.anonymNotes,
      commentednotes: searchConfig.commentedNotes,
      createdbefore: searchConfig.createdBefore,
      createdafter: searchConfig.createdAfter,
    });
    response.noteCounts = response.heatmap.map(p => p.count);
    let heatmapExpension = [];
    for(let { lat, lon, count } of response.heatmap) {
      if(lon >= 0) lon -= 360;
      else         lon += 360;
      heatmapExpension.push({ lat, lon, count });
    }
    response.heatmap = response.heatmap.concat(heatmapExpension);
  } catch (message) {
    console.error("Unexpected error while getting heatmap: ", message);
    let errorDiv = document.importNode(document.getElementById('error-template').content, true).firstElementChild;
    Translation.applyTranslationToNode(errorDiv);
    errorDiv.querySelector('.error-message').innerText = message;
    document.getElementById('errors-list').appendChild(errorDiv);
    return;
  }

  if(cancelable?.canceled) {
    console.info("loading canceled");
  } else {
    const timezone = getLocalTimeZoneString();

    let { heatmap, dbStats } = response;

    heatmapLayer.cfg.radius = Math.min(dlon, dlat)*2;
    heatmapLayer.setData({ data : heatmap, max: Math.max(...response.noteCounts)/5 });
    heatmapActiveLayerIndex = zoom;
    toggleHeatmapDisplayBasedOnZoom();

    const totalNoteCount = response.noteCounts.reduce((acc, v) => acc+v, 0);
    document.getElementById('notes-count-displayed').innerText = totalNoteCount.toLocaleString();
    document.getElementById('notes-count-total').innerText = dbStats.notes_count.toLocaleString() ;
    document.getElementById('comments-count-total').innerText = dbStats.comments_count.toLocaleString() ;
    const updateDateAsString = dbStats == null ? '-' : new Date(dbStats.update_date).toLocaleString() + '\n' + timezone;
    document.getElementById('latest-database-update-date').innerText = updateDateAsString;

    console.log("loaded heatmap (" + response.noteCounts.length + " samples, " + totalNoteCount + " notes)");
  }

}//reloadHeatmap()

function getLocalTimeZoneString() {
  // https://stackoverflow.com/questions/35546450/how-to-get-the-timezone-string-of-the-browser-in-javascript
  // return new window.Intl.DateTimeFormat().resolvedOptions().timeZone;
  const d = new Date();
  const dtf = Intl.DateTimeFormat(undefined, {timeZoneName: 'long'});
  return dtf.formatToParts(d).find((part) => part.type == 'timeZoneName').value;
}

function reloadExternalLinks() {
  { // set the openstreetmap link
    document.getElementById('goto-link-osm').href = `https://www.openstreetmap.org/#map=${map.getZoom()}/${map.getCenter().lat}/${map.getCenter().lng}&layers=N`;
  }
  { // set the NotesReview link
    // https://github.com/ENT8R/NotesReview/blob/main/app/js/query.js
    let reviewparams = '';
    reviewparams += `&bbox=${map.getBounds().toBBoxString()}&area=view&limit=100`;
    if(searchConfig.openedNotes    !== ONLY ) reviewparams += "&status="      + ["all", "closed"][['', EXCLUDED].indexOf(searchConfig.openedNotes)];
    if(searchConfig.anonymNotes    !== ''   ) reviewparams += "&anonymous="   + ["only", "hide"][[ONLY, EXCLUDED].indexOf(searchConfig.anonymNotes)];
    if(searchConfig.commentedNotes === ONLY ) reviewparams += "&uncommented=true"; // notesreview does not support "only non-commented"
    document.getElementById('goto-link-notesreview').href = `https://ent8r.github.io/NotesReview/?view=map${reviewparams}`;
  }
  { // update this page's link
    let decimals = 6;
    let round = (x) => Math.round(x*10**decimals)/10**decimals;
    let heatmapparams = '';
    heatmapparams += `?pos=${round(map.getCenter().lat)},${round(map.getCenter().lng)},${map.getZoom()}`;
    let flags = '';
    if(searchConfig.automaticReload) flags += 'r';
    if(searchConfig.openedNotes    !== '') flags += ["o", "O"][[ONLY, EXCLUDED].indexOf(searchConfig.openedNotes)];
    if(searchConfig.anonymNotes    !== '') flags += ["a", "A"][[ONLY, EXCLUDED].indexOf(searchConfig.anonymNotes)];
    if(searchConfig.commentedNotes !== '') flags += ["c", "C"][[ONLY, EXCLUDED].indexOf(searchConfig.commentedNotes)];
    //heatmapparams += flags == '' ? '' : '&flags=' + flags;
    heatmapparams += '&flags=' + flags ;
    if(searchConfig.precision != 5) heatmapparams += `&precision=${searchConfig.precision}`;
    if(searchConfig.createdAfter  !== undefined) heatmapparams += `&after=${searchConfig.createdAfter}`;
    if(searchConfig.createdBefore !== undefined) heatmapparams += `&before=${searchConfig.createdBefore}`;
    if(DEBUG_MODE) heatmapparams += '&debug';
    window.history.replaceState({}, window.location.origin, window.location.pathname + heatmapparams);
  }
}

function getMapPosFromURL(queryString) {
  let params = new URLSearchParams(queryString);
  if(params.has('pos')) {
    let pos = params.get('pos');
    let parts = pos.split(',');
    return {
      center: clampPositionIn180Range({ lat: parseFloat(parts[0]), lng: parseFloat(parts[1]) }),
      zoom: parseInt(parts[2]),
    };
  } else {
    return {
      center: new L.LatLng(46.2, 2.2),
      zoom: 4,
    };
  }
}

function loadSearchConfigFromURL(queryString) {
  let params = new URLSearchParams(queryString);
  if(params.has('after' )) searchConfig.createdAfter = params.get('after' );
  if(params.has('before')) searchConfig.createdAfter = params.get('before');
  if(params.has('precision')) searchConfig.precision = parseInt(params.get('precision'));
  if(params.has('flags')) {
    // if flags presents set ALL filters value.
    let flags = params.get('flags');
    if(flags.includes('o')) searchConfig.openedNotes    = ONLY;
    else if(flags.includes('O')) searchConfig.openedNotes    = EXCLUDED;
    else searchConfig.openedNotes = '';
    if(flags.includes('a')) searchConfig.anonymNotes    = ONLY;
    else if(flags.includes('A')) searchConfig.anonymNotes    = EXCLUDED;
    else searchConfig.anonymNotes = '';
    if(flags.includes('c')) searchConfig.commentedNotes = ONLY;
    else if(flags.includes('C')) searchConfig.commentedNotes = EXCLUDED;
    else searchConfig.commentedNotes = '';
    searchConfig.automaticReload = flags.includes('r');
  }

  let controlsPanel = document.getElementById('controls-panel');
  function setSelectedRadio(inputName, searchConfigValue) {
    let radio = controlsPanel.querySelector(`input[name=${inputName}][value="${searchConfigValue}"]`);
    if(radio !== null)
      radio.checked = true;
  }
  controlsPanel.querySelector('input[name=precision]'       ).value = searchConfig.precision;
  controlsPanel.querySelector('input[name=created-before]'  ).value = searchConfig.createdBefore;
  controlsPanel.querySelector('input[name=created-after]'   ).value = searchConfig.createdAfter;
  controlsPanel.querySelector('input[name=automatic-reload]').checked = searchConfig.automaticReload;
  setSelectedRadio('opened-notes   ', searchConfig.openedNotes   );
  setSelectedRadio('anonym-notes   ', searchConfig.anonymNotes   );
  setSelectedRadio('commented-notes', searchConfig.commentedNotes);
}

window.addEventListener('load', () => {
  var baseLayer = L.tileLayer(
    'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
      attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, Tiles © <a href="https://operations.osmfoundation.org/policies/tiles/">OSMF</a>',
      maxZoom: 18, minZoom: 3,
    }
  );

  var cfg = {
    radius: .5,
    maxOpacity: .8,
    scaleRadius: true,
    useLocalExtrema: false,
    latField: 'lat',
    lngField: 'lon',
    valueField: 'count',
    minZoom: 1,
  };

  heatmapLayer = new HeatmapOverlay(cfg);

  let mapPos = getMapPosFromURL(window.location.search);

  map = new L.Map('map-canvas', {
    center: mapPos.center,
    zoom: mapPos.zoom,
    layers: [ baseLayer, heatmapLayer ],
    maxBounds: [[-90, -10000.0], [90, 10000.0]],
  });
});

// double buffer reload :
// reload only 500ms after the map finished moving and
// if two queries 
let prevCancelable = [];
let currentTimeout = undefined;

function submitRefresh() {
  let cancelable = { canceled: false };
  let promise = reloadHeatmap(cancelable);
  prevCancelable.push([ promise, cancelable ]);
  document.getElementById('loading-icon').classList.remove('hidden');
  promise.then(() => {
    if(cancelable.canceled)
      return;
    for(let i = 0; i < prevCancelable.length; i++) {
      let [ otherPromise, otherCancelable ] = prevCancelable[i];
      if(promise == otherPromise) {
        prevCancelable.splice(0, i+1);
        break;
      } else {
        otherCancelable.canceled = true;
      }
    }
    if(prevCancelable.length == 0)
      document.getElementById('loading-icon').classList.add('hidden');
  });
}

function toggleMapOrControls() {
  let isMapActive = !document.getElementById('map-container').classList.toggle("hidden");
  document.getElementById('controls-panel').classList.toggle("visible");
  document.getElementById('toggle-map-controls-btn').innerText = Translation.getTranslation(isMapActive ? 'show_controls' : 'hide_controls');
}

function toggleHeatmapDisplayBasedOnZoom() {
  if(map.getZoom() > heatmapActiveLayerIndex + HEATMAP_LAYER_PERSISTANCE) {
    map.removeLayer(heatmapLayer);
  } else {
    map.addLayer(heatmapLayer);
  }
}

function goToStatsPage() {
  let url = STATS_PAGE_URL;
  let bbox = map.getBounds().toBBoxString();
  let since = new Date(); // by default only show the stats of the last 3 months
  since.setMonth(since.getMonth()-3);
  let pad2 = x => x.toString().padStart(2, '0');
  url += `?bbox=${bbox}`;
  url += `&since=${since.getFullYear()}-${pad2(since.getMonth()+1)}-${pad2(since.getDate())}`;
  // open the page in a new tab
  window.open(url, '_blank').focus();
}

window.addEventListener('load', () => {
  let controlsPanel = document.getElementById('controls-panel');

  function triggerSearchControlChange() {
    if(searchConfig.automaticReload)
      submitRefresh();
    reloadExternalLinks();
  }

  function initRadioControl(inputName, searchConfigPropertyName) {
    let radioElements = controlsPanel.querySelectorAll(`input[name=${inputName}]`);
    for(let e of radioElements) {
      // if it got checked, change the search config value
      e.addEventListener('change', () => {
        searchConfig[searchConfigPropertyName] = e.value ;
      });
      // if it was already checked, reset the search config value
      e.addEventListener('click', () => {
        if(searchConfig[searchConfigPropertyName] != e.value) return;
        e.checked = false;
        searchConfig[searchConfigPropertyName] = '';
        triggerSearchControlChange();
      });
    }
  }

  // update the search config on input change
  controlsPanel.querySelector('input[name=precision]'       ).addEventListener('change', function() { searchConfig.precision = this.value; });
  controlsPanel.querySelector('input[name=created-before]'  ).addEventListener('change', function() { searchConfig.createdBefore = this.value != '' ? this.value : undefined; });
  controlsPanel.querySelector('input[name=created-after]'   ).addEventListener('change', function() { searchConfig.createdAfter = this.value != '' ? this.value : undefined; });
  controlsPanel.querySelector('input[name=automatic-reload]').addEventListener('change', function() { searchConfig.automaticReload = this.checked; });
  initRadioControl('commented-notes', 'commentedNotes');
  initRadioControl('anonym-notes', 'anonymNotes');
  initRadioControl('opened-notes', 'openedNotes');

  for(let input of controlsPanel.querySelectorAll('input')) {
    input.addEventListener('change', () => triggerSearchControlChange());
  }

  let disableMoveEventHandlers = false;
  map.on('moveend', () => {
    if(disableMoveEventHandlers)
      return;

    disableMoveEventHandlers = true;
    let clampedCenter = clampPositionIn180Range(map.getCenter());
    map.setView(clampedCenter, map.getZoom(), { animate: false });
    disableMoveEventHandlers = false;

    reloadExternalLinks();
  });
  map.on('move', () => {
    if(searchConfig.automaticReload) {
      clearTimeout(currentTimeout);
      currentTimeout = setTimeout(submitRefresh, PANNING_AUTO_UPDATE_DELAY*1000);
    }
  });
  map.on('zoomend', () => toggleHeatmapDisplayBasedOnZoom());

  // process page load
  loadSearchConfigFromURL(window.location.search);
  if(!DEBUG_MODE && searchConfig.automaticReload)
    submitRefresh();
  reloadExternalLinks();
});
