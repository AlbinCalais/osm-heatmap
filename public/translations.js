/*
 * Small utility to translate a page dynamically.
 *
 * Lang files are to be stored in json files under a lang/ folder,
 * in hmtl elements that can be translated should have a "lang-key"
 * attribute, its value will be used as a key to search in the lang
 * file for a translation.
 * In javascript Translation#getTranslation can be used to retrieve
 * a translation at runtime. Translation.fetchLangFile can be used
 * to fetch a new lang file and apply it to the page dynamically.
 * 
 * Note that dynamic reloading *cannot apply* to elements that contain
 * text generated using Translation.getTranslation, these must be
 * reloaded by the script that generated theim.
 * 
 * Note that this is *not* the preferred way to do translations, it
 * is just simple enough for this project.
 */

const LANG_KEY_ATTRIBUTE = 'lang-key';
// will log the keys in the language file that are not used when
// applying a full translation to the page, the reimaining keys
// may be used elsewhere however (in scripts or in <template>
// elements for example).
const DEBUG_EXTRA_LANGUAGE_KEYS = false;

const Translation = {

    langFilesPath: null,     // the relative path to the xxlang.json files
    activeLanguageDict: {},
    activeLanguageTag: null, // 2 letter country code
    fallbackLanguageDict: {},

    setActiveLanguageDict(dict, lang, applyToPage=true) {
        this.activeLanguageDict = dict;
        this.activeLanguageTag = lang;

        if(applyToPage) {
            let extraKeys = this.applyTranslationToNode(document);
            
            if(extraKeys.length > 0) {
                console.warn("Extra language keys:", extraKeys);
            }
        }
    },

    applyTranslationToNode(node) {
        let extraKeys = DEBUG_EXTRA_LANGUAGE_KEYS ? Object.keys(this.activeLanguageDict) : [];

        for(let e of node.querySelectorAll(`[${LANG_KEY_ATTRIBUTE}]`)) {
            let key = e.attributes.getNamedItem(LANG_KEY_ATTRIBUTE).value;
            e.textContent = this.getTranslation(key);
            
            let ei = extraKeys.indexOf(key);
            if(ei != -1) extraKeys.splice(ei, 1);
        }

        return extraKeys;
    },

    getTranslation(translationKey) {
        if(translationKey in this.activeLanguageDict) {
            return this.activeLanguageDict[translationKey];
        } else if(translationKey in this.fallbackLanguageDict) {
            console.warn('missing translation for key', translationKey, ", using fallback");
            return this.fallbackLanguageDict[translationKey];
        } else {
            console.warn('mussing translation for key', translationKey, ', no available fallback!');
            return translationKey;
        }
    },

    tryFetch(lang) {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open('GET', `${this.langFilesPath}/${lang}.json`);
            xhr.send();
            // console.debug('fetching language file for', lang);
            xhr.onload = () => {
                if(xhr.status != 200)
                    reject(new Error(xhr.response));
                else
                    resolve(JSON.parse(xhr.responseText));
            }
        });
    },

    async fetchLangFile(lang, applyToPage=true) {
        try {
            let langDict = await this.tryFetch(lang);
            this.setActiveLanguageDict(langDict, lang, applyToPage);
        } catch (e) {
            console.error('Could not fetch lang file:', e.message);
        }
    },

    async fetchFirstAvailableLangFile(acceptedLanguages, defaultLang="en") {
        let alreadyTried = [];

        for(let langTag of acceptedLanguages) {
            let lang = langTag.substr(0, 2); // "en-EN" -> "en"
            if(alreadyTried.includes(lang))
                continue;
            alreadyTried.push(lang);
            try {
                let langDict = await this.tryFetch(lang);
                this.setActiveLanguageDict(langDict, lang, true);
                return;
            } catch {
                // no available lang file, continue with the next one
            }
        }

        console.debug("No available lang file corresponding to user's accepted languages, using", defaultLang, "as a fallback");
        this.fetchLangFile(defaultLang, true);
    },

    async loadPreferedLangFile(langFilesPath = "lang") {
        this.langFilesPath = langFilesPath;
        try {
            this.fallbackLanguageDict = await this.tryFetch("en");
        } catch (e) {
            console.error("No language dictionnary defined for english! There will be no fallback in case of a missing translation key");
        }
        await this.fetchFirstAvailableLangFile(navigator.languages);

        let self = this;
        function updateLangControls() {
            for(let e of document.getElementsByClassName('lang-select-control'))
                e.value = self.activeLanguageTag;
        }

        if(document.readyState != 'loading') {
            updateLangControls();
        } else {
            document.addEventListener('load', updateLangControls);
        }
    },

};