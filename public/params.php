<?php
/*
$hostSettings = file_get_contents("../secrets.txt");
$hostSettings = preg_split("/\r\n|\n|\r/", $hostSettings);

$connection = new PDO("mysql:$hostSettings[0];dbname=$hostSettings[1]", "$hostSettings[2]", "$hostSettings[3]");
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
*/

$config = parse_ini_file(__DIR__.'/../configuration.ini', true);

$connection = new PDO($config['DB_DSN'], $config['DB_USER'], $config['DB_PWD']);
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

/** For ValidateFormat to work with a regex */
const DATE_REGEX = "/^\d{4}-\d{2}-\d{2}$/";
const NUMBER_REGEX = "/^-?\d{1,20}$/";
const FLOAT_REGEX = "/^-?\d{1,20}(?:.\d{1,20})?$/";

function ValidateIsset($container, ...$names)
{
  foreach($names as $name) {
    if(!isset($container[$name])) {
      echo "Missing information for '" . $name ."'";
      http_response_code(406);
      exit(1);
    }
  }
}

function ValidateFormat($container, $name, $validFormat)
{
  ValidateIsset($container, $name);
  $matches = [];
  if(!preg_match($validFormat, $container[$name], $matches)) {
    echo "Invalid information given for '" . $name . "'";
    http_response_code(406);
    exit(1);
  }
  return count($matches) == 1 ? $matches[0] : $matches;
}
