<?php

include 'params.php';

function GetHeatmap() {
  $zoom    = isset($_GET['zoom']) ? ValidateFormat($_GET, 'zoom', NUMBER_REGEX) : 3;
  $precLon = isset($_GET['dlon']) ? ValidateFormat($_GET, 'dlon', FLOAT_REGEX) : 1;
  $precLat = isset($_GET['dlat']) ? ValidateFormat($_GET, 'dlat', FLOAT_REGEX) : 1;
  $onlyOpened      = isset($_GET['openednotes'   ]) && $_GET['openednotes'   ] == 'only';
  $onlyClosed      = isset($_GET['openednotes'   ]) && $_GET['openednotes'   ] == 'excluded';
  $onlyAnonym      = isset($_GET['anonymnotes'   ]) && $_GET['anonymnotes'   ] == 'only';
  $onlyNotAnonym   = isset($_GET['anonymnotes'   ]) && $_GET['anonymnotes'   ] == 'excluded';
  $onlyCommented   = isset($_GET['commentednotes']) && $_GET['commentednotes'] == 'only';
  $onlyCommentless = isset($_GET['commentednotes']) && $_GET['commentednotes'] == 'excluded';
  $createdAfter    = isset($_GET['createdafter' ]) ? ValidateFormat($_GET, 'createdafter' , DATE_REGEX) : False;
  $createdBefore   = isset($_GET['createdbefore']) ? ValidateFormat($_GET, 'createdbefore', DATE_REGEX) : False;

  if(isset($_GET['bbox'])) {
    // dirty regex
    $bbox = ValidateFormat($_GET, 'bbox', "/^(-?\d{1,20}(?:.\d{1,20})?),(-?\d{1,20}(?:.\d{1,20})?),(-?\d{1,20}(?:.\d{1,20})?),(-?\d{1,20}(?:.\d{1,20})?)$/");
    array_shift($bbox);
    $bbox = array_map('floatval', $bbox);
  } else {
    $bbox = [ -180, -90, +180, +90 ];
  }

  $queryArgs = [
    'dlon' => $precLon,
    'dlat' => $precLat,
    'bboxX1' => $bbox[0],
    'bboxY1' => $bbox[1],
    'bboxX2' => $bbox[2],
    'bboxY2' => $bbox[3],
  ];

  if($createdAfter !== false) $queryArgs['createdafter'] = $createdAfter;
  if($createdBefore !== false) $queryArgs['createdbefore'] = $createdBefore;

  switch( $zoom )
  {
    case '1':
    case '2':
      $zoom = 3 ;
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
      $field_select =
         'lonz_'.$zoom.' as lon,'
        .'latz_'.$zoom.' as lat' ;
      $field_lon = 'lonz_'.$zoom.'' ;
      $field_lat = 'latz_'.$zoom.'' ;
      break;
    default:
      $field_select =
         'round(longitude/:dlon)*:dlon as lon,'
        .'round(latitude /:dlat)*:dlat as lat' ;
      $field_lon = 'longitude' ;
      $field_lat = 'latitude' ;
      }

  $query = '
      select count(*) as count, lon, lat from (
        select '. $field_select
    .'  from notes
        where '.$field_lat.' >= 1*:bboxY1
          and '.$field_lat.' <= 1*:bboxY2'
    .'    and ( (
            (1*:bboxX1 < 1*:bboxX2) and '.$field_lon.'>=1*:bboxX1 and '.$field_lon.'<=1*:bboxX2 
          ) or (
            (1*:bboxX1 >= 1*:bboxX2) and ('.$field_lon.'>=1*:bboxX1 or '.$field_lon.'<=1*:bboxX2)
          ))'
    .( $onlyOpened ? ' and notes.opened' : '' )
    .( $onlyClosed ? ' and not notes.opened' : '' )
    .( $onlyAnonym ? ' and exists (select * from comments where comments.note_id = notes.note_id and uid is null and action = "opened")' : '' )
    .( $onlyNotAnonym ? ' and not exists (select * from comments where comments.note_id = notes.note_id and uid is null and action = "opened")' : '' )
    .( $onlyCommented ? ' and exists (select * from comments where comments.note_id = notes.note_id and action <> "opened")' : '')
    .( $onlyCommentless ? ' and not exists (select * from comments where comments.note_id = notes.note_id and action <> "opened")' : '')
    .( $createdAfter !== False ? ' and created_at > :createdafter' : '')
    .( $createdBefore !== False ? ' and created_at < :createdbefore' : '')
    .') N
    group by lon, lat;
  ';
  global $connection;
  $sth = $connection->prepare($query);
  $sth->execute($queryArgs);
  return $sth->fetchAll(PDO::FETCH_ASSOC);
}

function _debugPdoDumpParamsToStr( $sth )
{
  ob_start();
  $sth->debugDumpParams();
  $r = ob_get_contents();
  ob_end_clean();
  return $r;
}

function GetDatabaseStats() {
  global $connection;
  $stats = [
    'update_date' => null,
    'notes_count' => null,
    'comments_count' => null,
  ];
  // assuming that the `updates` table was only inserted UTC_TIMESTAMP()
  // and that the computer hosting the database did not change time zone 
  $request = $connection->prepare('select date_format(max(date),"%c/%d/%Y %T UTC") as date from updates;');
  $request->execute();
  $row = $request->fetch(PDO::FETCH_ASSOC);
  $stats['update_date'] = $row['date'];
  $stmt = $connection->prepare('select count(*) from notes');
  $stmt->execute();
  $stats['notes_count'] = $stmt->fetchColumn();
  $stmt = $connection->prepare('select count(*) from comments');
  $stmt->execute();
  $stats['comments_count'] = $stmt->fetchColumn();
  return $stats;
}

echo json_encode([
  'dbStats' => GetDatabaseStats(),
  'heatmap' => GetHeatmap(),
],
  // Php 7.x compatibility (without it will be strings and javascript miss some calculations...)
 JSON_NUMERIC_CHECK
);
