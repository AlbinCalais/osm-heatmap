/*
 * Create tables for osm-notes-heatmap
 */

/*
DROP DATABASE IF EXISTS osm_notes ;
create database osm_notes ;
use osm_notes ;
*/

create table notes (
  note_id INT NOT NULL PRIMARY KEY,
  created_at TIMESTAMP,
  closed_at TIMESTAMP,
  longitude FLOAT,
  latitude FLOAT,
  opened BOOLEAN,
  
  KEY `notes_opened` (`opened`)

  -- useless indexes
  -- KEY `notes_longitude` (`longitude`),
  -- KEY `notes_latitude` (`latitude`),

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

/* environ 20 Mo sur le disque pour les 2 colonnes (lon & lat) avec 3 169 369 rows */
ALTER TABLE `notes`
  ADD COLUMN `lonz_3` FLOAT as (round(longitude/1.2692775974025974) * 1.2692775974025974) STORED ,
  ADD COLUMN `latz_3` FLOAT as (round(latitude /1.3256108783442524) * 1.3256108783442524) STORED ,
  ADD COLUMN `lonz_4` FLOAT as (round(longitude/0.6346387987012988) * 0.6346387987012988) STORED ,
  ADD COLUMN `latz_4` FLOAT as (round(latitude /0.6910388126347027) * 0.6910388126347027) STORED ,
  ADD COLUMN `lonz_5` FLOAT as (round(longitude/0.305419921875) * 0.305419921875) STORED ,
  ADD COLUMN `latz_5` FLOAT as (round(latitude /0.3332454019157348) * 0.3332454019157348) STORED ,
  ADD COLUMN `lonz_6` FLOAT as (round(longitude/0.18234025186567168) * 0.18234025186567168) STORED ,
  ADD COLUMN `latz_6` FLOAT as (round(latitude /0.19889476344038282) * 0.19889476344038282) STORED ,
  ADD COLUMN `lonz_7` FLOAT as (round(longitude/0.06108398437500001) * 0.06108398437500001) STORED ,
  ADD COLUMN `latz_7` FLOAT as (round(latitude /0.06661201441622595) * 0.06661201441622595) STORED ;
/*
Do not add those indexes, queries will be slower (eg. 3.9s vs 0.8s) :(
ALTER TABLE `notes` 
  ADD INDEX `notes_z7` (`lonz_7` ASC, `latz_7` ASC) VISIBLE ,
  ADD INDEX `notes_z6` (`lonz_6` ASC, `latz_6` ASC) VISIBLE ,
  ADD INDEX `notes_z5` (`lonz_5` ASC, `latz_5` ASC) VISIBLE ,
  ADD INDEX `notes_z4` (`lonz_4` ASC, `latz_4` ASC) VISIBLE ,
  ADD INDEX `notes_z3` (`lonz_3` ASC, `latz_3` ASC) VISIBLE ;
*/

create table comments (
  comment_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  note_id INT,
  uid INT,
  date TIMESTAMP NOT NULL,
  text LONGTEXT,
  action enum('opened', 'closed', 'commented', 'reopened', 'hidden'),

  FOREIGN KEY (note_id) REFERENCES notes(note_id),

  KEY `comments_note_id` (`note_id`),
  KEY `comments_action` (`action`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

create table updates (
  date TIMESTAMP NOT NULL PRIMARY KEY
) ENGINE=InnoDB ;
