# from dotenv import load_dotenv
# load_dotenv()

import argparse
import dateutil.parser
from tqdm import tqdm
from lxml import etree

import osm_db

# Eavily inspired by https://github.com/ENT8R/notesreview-api/blob/main/db/scripts/import.py


def insert(file, first, last):
  notes = []
  all_comments = []
  BATCH_SIZE = 50000
  total_notes = 0
  total_comments = 0
  i = 0
  it = 0

  parsed_iterator = etree.iterparse(file, tag='note')

  print("Skipping first", first)
  print("Running from", first, "to", last)
  print('Batch size =', BATCH_SIZE)
  for _, element in tqdm(parsed_iterator):
    if it < first:
      continue
    if last != -1 and it >= last:
      break
    attributes = element.attrib
    comments = parse(element)
    #if len(comments) == 0:
    #  print("Strange note found (no comments, no opener)")
    #  continue
    created_at = dateutil.parser.parse(attributes['created_at'])
    closed_at = None if 'closed_at' not in attributes else dateutil.parser.parse(attributes['closed_at'])
    note = {
        'note_id': int(attributes['id']),
        'longitude': float(attributes['lon']),
        'latitude': float(attributes['lat']),
        'opened': 'closed_at' not in attributes,
        'closed_at': closed_at,
        'created_at': created_at,
    }
    for c in comments:
      c['note_id'] = note['note_id']
    notes.append(note)
    all_comments.extend(comments)
    element.clear()
    i += 1
    it += 1
    if i >= BATCH_SIZE:
      osm_db.send_notes_and_comments(notes, all_comments)
      total_notes += len(notes)
      total_comments += len(all_comments)
      print(' step:', total_notes, 'notes and', total_comments, 'comments')
      notes = []
      all_comments = []
      i = 0
  # end for parsed_iterator
  osm_db.send_notes_and_comments(notes, all_comments)
  osm_db.send_update_date()
  total_notes += len(notes)
  total_comments += len(all_comments)
  print("Finished!")
  print("Total imported", total_notes, "notes and", total_comments, "comments")

# Parse the comments and extract only the useful information
def parse(note):
  comments = []
  for element in note:
    attributes = element.attrib

    if not attributes['action'] in ['opened', 'closed', 'commented', 'reopened', 'hidden']:
      raise ValueError('Unknow "action" : "' + attributes['action'] + '"')

    comment = {
      'date': dateutil.parser.parse(attributes['timestamp']),
      'action': attributes['action'],
      'text': element.text,
      'uid': None,
    }
    if 'uid' in attributes: comment['uid'] = int(attributes['uid'])

    comments.append(comment)
  return comments

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Import notes from a notes dump.')
  parser.add_argument('file', type=str, help='path to the file which contains the notes dump')
  parser.add_argument('--first', type=int, help='first note index, use for partial import', default=0)
  parser.add_argument('--last', type=int, help='1 more than the last note index, use for partial import', default=-1)
  parser.add_argument('--empty', action='store_true', help='empty the database before filling it back up again')
  args = parser.parse_args()
  if args.empty:
    print('Deleting database...')
    osm_db.empty_database()
  insert(args.file, args.first, args.last)
