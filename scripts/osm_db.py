# Not Python3 compatible
#import MySQLdb
# https://pymysql.readthedocs.io
import pymysql.cursors
import configparser
import os

# Impl note: For some reason the database connection does
# not last long enough for some of the queries, terminating
# with a strange error.
# To avoid crashes the connection is opened and disposed of
# for each batch request.

# Read INI file without "section"
config = configparser.ConfigParser()
file = os.path.join( os.path.dirname(__file__), '../configuration.ini')
with open( file ) as stream:
    config.read_string("[CONFIG]\n" + stream.read())  # create a fake section "CONFIG"
dsn = config.get('CONFIG','DB_DSN')
dsn = dsn.replace('mysql:', "", 1)
# and fill "connectParams"
connectParams = dict(entry.split('=') for entry in dsn.split(';'))
connectParams['user'] =  config.get('CONFIG','DB_USER')
connectParams['password'] =  config.get('CONFIG','DB_PWD')
connectParams['database'] = connectParams['dbname']
del connectParams['dbname']

def open_connection():
  global connectParams
  client = pymysql.connect(**connectParams)
  cursor = client.cursor()
  # Access denied; you need (at least one of) the SUPER privilege(s) for this operation
  #cursor.execute('set global max_allowed_packet=67108864')
  ##cursor.execute('set global net_read_timeout=31536000')
  cursor.execute('SET AUTOCOMMIT=0')
  cursor.execute("SET time_zone = 'Etc/UTC'")

  return client, cursor

def close_connection(client, cursor):
  #print("done")
  cursor.close()
  client.commit()
  client.close()

def empty_database():
  #print("Emptying database...")
  client, cursor = open_connection()
  client.begin()
  cursor.execute("delete from comments where 1;")
  cursor.execute("delete from notes where 1;")
  close_connection(client, cursor)
  #print("Database emptied.")

def send_notes_and_comments(notes, comments):
  client, cursor = open_connection()
  client.begin()
  cursor.executemany("""insert into notes (note_id, created_at, closed_at, longitude, latitude, opened)
                        values (%(note_id)s, %(created_at)s, %(closed_at)s, %(longitude)s, %(latitude)s, %(opened)s);""", notes)
  cursor.executemany("""insert into comments (note_id, uid, date, text, action)
                        values (%(note_id)s, %(uid)s, %(date)s, %(text)s, %(action)s);""", comments)
  client.commit()
  close_connection(client, cursor)

def send_update_date():
  client, cursor = open_connection()
  cursor.execute("insert into updates (date) values (utc_timestamp());")
  close_connection(client, cursor)
