#!/usr/bin/env php
<?php
/*
 * update.php
 * Use the OSM Notes API to update the local Notes database.
 */

/*
 * With debug on:
 * - api results will be stored on disk and reused for futher reading
 * @var bool $debug
 */
$debug = false ;

$apiurl = 'https://api.openstreetmap.org/api/0.6/notes/search.json' ;
$limit = 1000 ;

$stats = [
    'from' => null,
    'last_date' => null,
    'limit' => $limit ,
    'notes_read' => 0,
    'notes_found' => 0,
    'notes_created' => 0,
    'notes_updated' => 0,
    'comments_read' => 0,
    'comments_found' => 0,
    'comments_created' => 0,
];

// Read settings


$config = parse_ini_file(__DIR__.'/../configuration.ini', true);

$db = new PDO($config['DB_DSN'], $config['DB_USER'], $config['DB_PWD']);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

/**
 * Select last comment's date
 * @var \DateTime $previous_date
 */

$query = $db->prepare('SELECT max(`date`) FROM comments');
$query->execute();
$rows = $query->fetch();
if($rows[0] == null) {
    echo "No comment in database, import a dump first\n";
    exit();
}
$previous_date = new \DateTime( $rows[0] );
if( ! $previous_date )
{
    throw new \RuntimeException('Could not find last comment date to set $previous_date.');
}
$stats['from'] = $previous_date->format( 'Y-m-d H:i:s' );

if( $debug )
    echo 'update initial from: ',$previous_date->format( 'Y-m-d H:i:s' ),"\n" ;

// Boucle tant que parse_result() retourne une date

while( $previous_date )
{
    $url = $apiurl.'?from='.$previous_date->format(DateTime::ATOM)
    . '&closed=-1'
    . '&order=oldest'
    . '&sort=updated_at'
    . '&limit='.$limit ;

    $debug_file = sha1($url).'.json' ;
    if( $debug && file_exists($debug_file) )
    {
        $json = file_get_contents( $debug_file );
    }
    else
    {
        $json = file_get_contents( $url );
        if( $debug )
            file_put_contents( $debug_file, $json );
        sleep( 2 );
    }

    $previous_date = parse_result( json_decode($json) );
    if( $previous_date == null )
    {
        if( $debug )
            echo 'END',"\n";
    }
    else
    {
        $stats['last_date'] = $previous_date->format( 'Y-m-d H:i:s' ) ;
        if( $debug )
            echo $previous_date->format( 'Y-m-d H:i:s' ),"\n";
    }
}

$sth = $db->prepare('INSERT INTO updates (`date`) VALUES (UTC_TIMESTAMP())')->execute();

if( $debug )
    var_export($stats);

/**
 * 
 */
function parse_result( $json )
{
    global $db, $debug, $stats ;

    // 2022-06-06 00:00:49 UTC
    $NOTE_DATE_FORMAT = 'Y-m-d H:i:s e' ;
    $lastDate = null ;

    $sth_notes_select = $db->prepare('SELECT * FROM notes where note_id=:note_id');
    $sth_notes_insert = $db->prepare('INSERT INTO notes (note_id,created_at,closed_at,longitude,latitude,opened) VALUES (:note_id,:created_at,:closed_at,:longitude,:latitude,:opened)');
    $sth_notes_update = $db->prepare('UPDATE notes SET opened=:opened, closed_at=:closed_at WHERE note_id=:note_id');
    $sth_comments_select = $db->prepare('SELECT * FROM comments where note_id=:note_id order by `date` ASC');
    $sth_comments_insert = $db->prepare('INSERT INTO comments (note_id,date,uid,action,text) VALUE (:note_id,:date,:uid,:action,:text)');

    $features_count = count($json->features) ;
    if( $debug )
        echo 'notes count: ',$features_count,"\n";

    if( $features_count == 0 || $features_count == 1 )
        return null ;

    $db->beginTransaction();

    foreach( $json->features as $feature )
    {
        $stats['notes_read'] ++ ;

        $id = $feature->properties->id ;
        $date_created = $feature->properties->date_created ;
        $closed_at = isset($feature->properties->closed_at) ? $feature->properties->closed_at : null ;
        $closed_at_str = $closed_at == null ? null : (\DateTime::createFromFormat($NOTE_DATE_FORMAT, $closed_at))->format('Y-m-d H:i:s') ;
        $status = $feature->properties->status ;
        $comments = $feature->properties->comments ;

        // Has a known status
        if( ! in_array( $status, ['closed','open'] ) )
        {
            var_export( $feature );
            throw new \RuntimeException('Note '.$id.' unknow status' );
        }

        // Notes table

        $sth_notes_select->execute(['note_id'=>$id]);
        $note_row = $sth_notes_select->fetch(PDO::FETCH_OBJ);

        if( ! $note_row )
        {
            $stats['notes_created'] ++ ;
            $note = [
                'note_id' => $id,
                'created_at' => (\DateTime::createFromFormat($NOTE_DATE_FORMAT, $date_created))->format('Y-m-d H:i:s'),
                'closed_at' => $closed_at_str,
                'longitude' => $feature->geometry->coordinates[0],
                'latitude' => $feature->geometry->coordinates[1],
                'opened' => ($status == 'open' ? 1 : 0 ),
            ];
            $sth_notes_insert->execute($note);
        }
        else
        {
            $stats['notes_found'] ++ ;

            // Update status
            $note_was_updated = false;
            if(
                ( $status=='closed' && $note_row->opened )
                || ( $status=='open' && (!$note_row->opened) )
            )
            {
                $note_was_updated = true;
                $note_row->opened = $status=='closed' ? 0 : 1;
            }
            if( $note_row->closed_at != $closed_at_str ) {
                $note_was_updated = true;
            }

            if( $note_was_updated ) {
                $sth_notes_update->execute([
                    'note_id' => $id,
                    'closed_at' => $closed_at_str,
                    'opened' => $note_row->opened,
                ]);
                $stats['notes_updated'] ++ ;
            }
        }

        // Comments table
        //
        // Comme il peut y avoir des doublons on ne va pas comparer les données
        // mais simplement alimenter la liste de commentaires.

        $comments_count = count($comments) ;
        $stats['comments_read'] += $comments_count ;

        $sth_comments_select->execute(['note_id'=>$id]);
        $rows = $sth_comments_select->fetchAll();
        $row_count = count($rows);

        if( $row_count == $comments_count )
        {
            // comments list is up-to-date
            $stats['comments_found'] += $comments_count ;
        }
        else
        {
            for( $i=$row_count; $i<$comments_count; $i++)
            {
                $stats['comments_created'] ++ ;
                $comment =& $comments[$i];
                $row = [
                    'note_id'=> $id,
                    'date' => (\DateTime::createFromFormat($NOTE_DATE_FORMAT,$comment->date))->format('Y-m-d H:i:s'),
                    'uid' => $comment->uid ?? null,
                    'action' => $comment->action ?? null,
                    'text' => $comment->text,
                ];
                $sth_comments_insert->execute( $row );
            }
        }


        // Compute last date

        if( $comments_count == 0 )
        {
            if( $closed_at )
                $lastDate = $closed_at ;
            else
                $lastDate = $date_created ;
        }
        else
        {
            $lastDate = $comments[ $comments_count -1 ]->date ;
        }
        $lastDate = \DateTime::createFromFormat(
            $NOTE_DATE_FORMAT,
            $lastDate
        );

    }// each features

    $db->commit();

    return $lastDate ;
}
