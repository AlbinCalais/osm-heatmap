# OSM Notes Heatmap

Ce projet a pour objectif de dresser une heatmap des notes utilisateurs d'[OpenStreetMap](https://osm.org).
Pour cela on utilise l'api d'osm pour récuperer les notes et les stoquer dans une base de données.
La heatmap est générée dans le navigateur à partir des données récupérées dans le end-point `api.php`.

![](https://i.creativecommons.org/l/by-sa/4.0/88x31.png) Ce programme est mis à disposition selon les termes de la licence [Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/).

<div align="center">
![screenshot](screenshot.jpg)]

Vous pouvez utiliser le service [notes-heatmap.openstreetmap.fr](https://notes-heatmap.openstreetmap.fr/?pos=45.64,2.56,6&flags=oacr) dont les moyens techniques sont gracieusement fournis par l'association [OpenStreetMap France](https://openstreetmap.fr).
</div>

> **Pour les utilisateurs d'[OpenSwitchMaps](https://github.com/tankaru/OpenSwitchMaps)** :\
> Le lien vers le site a été ajouté (merci à tankaru sur github!) mais n'est pas affiché par défaut, vous pouvez l'activer dans la page de configuration. *pour l'instant les extensions firefox/chrome ne sont pas à jour*\
> **Pour les utilisateurs d'[osm smart menu](https://github.com/jgpacker/osm-smart-menu)** :\
> Vous pouvez ajouter `https://notes-heatmap.openstreetmap.fr/?pos={latitude},{longitude},{zoom}` à la liste des sites. Plus d'info [ici](https://wiki.openstreetmap.org/wiki/OSM_Smart_Menu) pour savoir comment faire.

[TOC]

## Crédits

- [OpenStreetMap](https://osm.org), [API OSM Notes](https://wiki.openstreetmap.org/wiki/API_v0.6#Map_Notes_API)
- [Heatmap.js](https://www.patrick-wied.at/static/heatmapjs/) par Patrick Wied
- [Php](https://php.net), [Python](https://python.org), [Javascript](https://developer.mozilla.org/fr/docs/Web/JavaScript), [SQL](https://blog.ansi.org/sql-standard-iso-iec-9075-2023-ansi-x3-135/)

## API heatmap_query

Sur `public/api.php`.

| Paramètre | Format | Utilité |
| --------- | ---- | ------- |
| zoom | int | the leaflet map zoom level, used instead of dlon/dlat to retrieve notes if possible |
| dlon | float | longitude delta, points at distance less than the delta are merged together |
| dlat | float | latitude delta, points at distance less than the delta are merged together |
| bbox | float,float,float,float | points outside of the bounding box are ignored |
| openednotes | "only", "excluded" | filter out closed or opened notes |
| anonymnotes | "only", "excluded" | filter out (non-)anonym notes |
| commentednotes | "only", "excluded" | filter out commentless or commented notes |
| createdbefore | date | filter out notes created after |
| createdafter | date | filter out notes created before |

Note: La bounding box doit être dans l'intervalle (-90,-180)..(+90,+180). Il est possible que west>east si la bounding box dépasse. 

## Scripts

- `db.sql`: créé les tables dans la base de données
- `import.py`: importe un dump des notes dans la bdd (python3)
- `update.php`: met à jour la bdd à partir de l'api OSM `/notes/search` (testé avec php7.4 -> php8.3)

## Installation

Créer une base de données, un user et créer les tables avec le fichier `scripts/db.sql`.

Remplissez le fichier `secrets.txt`, il contient, dans l'ordre :
- l'addresse du serveur mysql (host ou socket),
- le nom de la base de données,
- l'utilisateur mysql
- et son mot de passe.
Pour l'addresse du serveur mysql, vous pouvez utiliser une addresse ip `host=127.0.0.1` ou un socket unix `unix_socket=/var/run/mysqld/mysqld.sock`.

Pour l'import c'est Python qui est utilisé, installer les dépendances avec `pip` (`sudo apt install python3-pip`).
```bash
cd scripts
pip install -r requirements.txt
```

Télécharger le dump `planet-notes-latest.osn.bz2` depuis https://planet.openstreetmap.org/notes,
le décompresser, puis lancer `python3 scripts/import.py planet-notes-latest.osn` (_penser à supprimer ces 2 volumineux fichiers_).

> **Attention ! il y a des dates qui ne sont pas valides commme '2019-03-31 02:xx:xx'** à cause du changement d'heure 2 fois pas an :\
> You are most likely using a EU-like timezone, in particular, CET (I strongly suggest not to use tz on databases, and just use UTC, it is just easier). EU (and other countries) changed the clocks from 2 -> 3 am that Sunday for summertime adjustment, so it is not possible, if using such a TZ, to insert a '2019-03-31 02:XX:XX ' data, as such data is invalid for that timezone.\
> Il faut donc avoir soit un host en timezone sans changement d'heure (ex `Etc/UTC`) soit un serveur mysql/mariadb avec la timezone `Etc/UTC` et pour ça remplir les [tables de fuseaux horaires mysql](https://runebook.dev/fr/docs/mariadb/time-zones/index).

Une fois le script exécuté la base de données est remplie.
```bash
# 2022-08-06, Core i7
time python3 ./import.py --empty ~/DATA/OSM/planet-notes-latest.osn
Total imported 3230029 notes and 6900406 comments
real 11m29,509s
user 9m53,570s
sys	 0m1,468s
```

Ensuite, la mise à jour et le site utilise Php.

Pour mettre à jour automatiquement la base exécuter périodiquement `update.php`.
```
# crontab -l
30 * * * * /var/www/notes-heatmap/scripts/update.php
```

Créer un vhost dont le root document est `./public` et que les fichiers `.php` sont passés à `php`.

Et voilà.

## API OSM Notes

Documentation de l'[API OSM Notes](https://wiki.openstreetmap.org/wiki/API_v0.6#Map_Notes_API).

Le [feed](https://api.openstreetmap.org/api/0.6/notes/feed) n'est pas utilisé car il n'est pas assez long et ne remonte pas tout. C'est donc le [search](https://api.openstreetmap.org/api/0.6/notes/search) qui est utilisé périodiquement pour mettre à jour la bdd.

### Règles de gestion des données Notes

Pour chaque note il n'y a qu'un seul commentaire avec `action=="opened"`. Utilisé pour sélectionner les notes anonymes ou non: on ne sélectionne que les notes avec un commentaire avec `action=="opened"` et un `uid=null` ou pas.

Les commentaires semblent fournis dans l'ordre chronologique. On prend donc comme règle pour la mise à jour des notes que les commentaires fournis par l'api sont ordonnés chronologiquement. Alors pour chaque note on compare le nombre de commentaires fourni par l'api et le nombre en bdd, si le nombre de commentaires en provenance de l'api est supérieur à celui en bdd on ajoute dans la bdd le différentiel ordonné. C'est plus facile et rapide.

### Commentaires en doublon

Il peut y avoir des doublons de commentaires (note's comments) comme on le voit sur cette note:

https://api.openstreetmap.org/api/0.6/notes/3270264.json

```json
{

    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [
            -0.4895231,
            43.8835976
        ]
    },
    "properties": {
        "id": 3270264,
        "url": "https://api.openstreetmap.org/api/0.6/notes/3270264.json",
        "reopen_url": "https://api.openstreetmap.org/api/0.6/notes/3270264/reopen.json",
        "date_created": "2022-07-16 20:17:51 UTC",
        "status": "closed",
        "closed_at": "2022-07-17 08:31:07 UTC",
        "comments": [
            {
                "date": "2022-07-16 20:17:51 UTC",
                "uid": 1790906,
                "user": "LucN31",
                "user_url": "https://api.openstreetmap.org/user/LucN31",
                "action": "opened",
                "text": "Unable to answer \"How does this road cross the barrier here?\" for https://osm.org/node/9476828590 via StreetComplete 45.0:\n\nIl n'y a plus de barrière.",
                "html": "<p>Unable to answer \"How does this road cross the barrier here?\" for <a href=\"https://osm.org/node/9476828590\" rel=\"nofollow noopener noreferrer\">https://osm.org/node/9476828590</a> via StreetComplete 45.0:</p>\n\n<p>Il n'y a plus de barrière.</p>"
            },
            {
                "date": "2022-07-17 08:31:07 UTC",
                "uid": 128938,
                "user": "Patchanka",
                "user_url": "https://api.openstreetmap.org/user/Patchanka",
                "action": "commented",
                "text": "bonjour\nj'ai supprimé la cloture qui ne correspondait pas au terrain (mais représentait (à tort) la limite de parcelle)",
                "html": "<p>bonjour\n<br />j'ai supprimé la cloture qui ne correspondait pas au terrain (mais représentait (à tort) la limite de parcelle)</p>"
            },
            {
                "date": "2022-07-17 08:31:07 UTC",
                "uid": 128938,
                "user": "Patchanka",
                "user_url": "https://api.openstreetmap.org/user/Patchanka",
                "action": "closed",
                "text": "bonjour\nj'ai supprimé la cloture qui ne correspondait pas au terrain (mais représentait (à tort) la limite de parcelle)",
                "html": "<p>bonjour\n<br />j'ai supprimé la cloture qui ne correspondait pas au terrain (mais représentait (à tort) la limite de parcelle)</p>"
            }
        ]
    }
}
```

### Notes search /api/0.6/notes/search

L'api [search](https://api.openstreetmap.org/api/0.6/notes/search) est utilisé périodiquement pour mettre à jour la base de donnée.

Exemple au format JSON:

https://api.openstreetmap.org/api/0.6/notes/search.json?from=2022-06-13%2010:51:53&closed=-1&order=oldest&sort=updated_at&limit=1000

Résultat (tronqué) avec une note ouverte et une fermée:
```json
{

    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    15.9213012,
                    45.7853001
                ]
            },
            "properties": {
                "id": 3222654,
                "url": "https://api.openstreetmap.org/api/0.6/notes/3222654.json",
                "comment_url": "https://api.openstreetmap.org/api/0.6/notes/3222654/comment.json",
                "close_url": "https://api.openstreetmap.org/api/0.6/notes/3222654/close.json",
                "date_created": "2022-06-13 10:52:01 UTC",
                "status": "open",
                "comments": [
                    {
                        "date": "2022-06-13 10:52:01 UTC",
                        "uid": 172435,
                        "user": "mnalis ALTernative",
                        "user_url": "https://api.openstreetmap.org/user/mnalis%20ALTernative",
                        "action": "opened",
                        "text": "conn e w",
                        "html": "<p>conn e w</p>"
                    }
                ]
            }
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.7059035,
                    51.3032526
                ]
            },
            "properties": {
                "id": 3222537,
                "url": "https://api.openstreetmap.org/api/0.6/notes/3222537.json",
                "reopen_url": "https://api.openstreetmap.org/api/0.6/notes/3222537/reopen.json",
                "date_created": "2022-06-13 09:42:03 UTC",
                "status": "closed",
                "closed_at": "2022-06-13 10:53:19 UTC",
                "comments": [
                    {
                        "date": "2022-06-13 09:42:03 UTC",
                        "action": "opened",
                        "text": "In openfietsmap is het Tjokkerspad als verhard weergegeven terwijl het onverhard is.",
                        "html": "<p>In openfietsmap is het Tjokkerspad als verhard weergegeven terwijl het onverhard is.</p>"
                    },
                    {
                        "date": "2022-06-13 10:53:19 UTC",
                        "uid": 10846743,
                        "user": "H4N5-antw",
                        "user_url": "https://api.openstreetmap.org/user/H4N5-antw",
                        "action": "closed",
                        "text": "Zie bijvoorbeeld https://www.mapillary.com/app/?pKey=946378282723204",
                        "html": "<p>Zie bijvoorbeeld <a href=\"https://www.mapillary.com/app/?pKey=946378282723204\" rel=\"nofollow noopener noreferrer\">https://www.mapillary.com/app/?pKey=946378282723204</a></p>"
                    }
                ]
            }

        }
    ]
}
```
